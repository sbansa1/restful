import json
from app.api.models import User
import pytest


def test_add_user(test_app, test_database):
    """Tests the users"""

    data = dict(username="Saurabh.bnss0123", email="Saurabh.bnss0123@gmail.com")

    client = test_app.test_client()
    response = client.post(
        "/users", data=json.dumps(data), content_type="application/json"
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 201
    assert "Saurabh.bnss0123@gmail.com was added!" in data.get("message")


def test_user_already_exists(test_app, test_database):
    """Duplicate User"""

    user_data = dict(username="Saurabh", email="Saurabh.bnss0123@gmail.com")

    client = test_app.test_client()

    client.post("/users", data=user_data, content_type="application/json")

    response = client.post(
        "/users", data=json.dumps(user_data), content_type="application/json"
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


""""
def test_single_user(test_app, test_database, add_user):
    user = add_user("saurabh", "Saurabh.bnss0123@gmail.com")
    client = test_app.test_client()
    respone = client.get("/users/{0}".format(user.id))
    data = json.loads(respone.data.decode())
    assert respone.status_code == 404
    assert "saurabh" in data.get("username")
    assert "Saurabh.bnss0123@gmail.com" in data.get("email")
"""

"""
def test_single_user_incorrect_id(test_app, test_database):

    client = test_app.test_client()
    resp = client.get("/users/{0}".format(int(999)))
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data.get("message")
"""


def test_all_users(test_app, test_database, add_user):

    test_database.session.query(User).delete()
    add_user("madbala", "madhubala.gadodia@gmail.com")
    add_user("micheal", "micheal@he.com")
    client = test_app.test_client()
    resp = client.get("/users")
    data = json.loads(resp.data.decode())
    assert len(data) == 2
    assert "madhubala.gadodia@gmail.com" in data[0]["email"]
    assert "madbala" in data[0]["username"]
    assert "micheal" in data[1]["username"]
    assert "micheal@he.com" in data[1]["email"]


def test_delete_user(test_app, test_database, add_user):
    """Test case for delete user"""
    test_database.session.query(User).delete()
    user = add_user("Koby", "Koby.bryant@gmail.com")
    client = test_app.test_client()
    response = client.get("/users")
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert len(data) == 1
    response_two = client.delete(f"/users/{user.id}")
    data = json.loads(response_two.data.decode())
    assert response_two.status_code == 200
    assert "user deleted" in data.get("message")
    response_three = client.get("/users")
    data = json.loads(response_three.data.decode())
    assert response_three.status_code == 200
    assert len(data) == 0


""""
def test_remove_incorrect_id(test_app,test_database):

    client = test_app.test_client()
    response = client.delete("/users/999")
    data =  json.loads(response.data.decode())
    assert response.status_code == 404
    assert "User Not Found" in data.get("message")
"""


def test_update_user(test_app, test_database, add_user):
    """Tests the put endpoint"""
    test_database.session.query(User).delete()
    user = add_user(username="saurabh", email="saurabh.bnss0123@gmail.com")
    client = test_app.test_client()
    data = dict(username="saurabh12345", email="sa12@gmail.com")
    response = client.put(
        f"/users/{user.id}", data=json.dumps(data), content_type="application/json"
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert "User Updated" in data.get("message")


"""
def test_invalid_payload(test_app,test_database):
    data = {}
    client = test_app.test_client()
    response = client.put(f"/users/1", data=json.dumps(data),content_type="application/json")
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert "invalid payload" in data['message']

def test_user_invalid_json_keys(test_app,test_database):

    payload = dict(email="saurabh.bnss0123@gmail.com")
    client = test_app.test_client()
    response = client.put("/users/1",data=json.dumps(payload),content_type="application/json")
    data = json.loads(response.data.decode())
    assert response.status_code == 404
    assert "invalid payload" in data.get("message")

def test_invalid_user(test_app,test_database):

    client = test_app.test_client()
    payload = dict(username="saurabh12345",email="sa12@gmail.com")
    resp = client.put("users/999",data=json.dumps(payload),content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User Not Found" in data.get("message")
"""

"""
@pytest.mark.parametrize("user_id, payload, status_code, message",[[1, {}, 400, "User not Found"],
    [1, {"email": "saurabh.bnss0123@gmail.com"}, 400, "User not Found"],
    [999, {"username": "saurabh12345", "email": "sa12@gmail.com"}, 400, "User not Found"],
])
def test_update_user_invalid(test_app, test_database, user_id, payload, status_code, message):
    test_database.session.query(User).delete()
    client = test_app.test_client()
    resp = client.put(
        f"/users/{user_id}/",
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]
"""
